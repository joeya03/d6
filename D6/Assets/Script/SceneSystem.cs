using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSystem : MonoBehaviour
{
    public string LevelToLoad;

    //loads the scene of specified scene
    public void LoadScene()
    {
        Cursor.visible = true;
        SceneManager.LoadScene(LevelToLoad);
    }
}
