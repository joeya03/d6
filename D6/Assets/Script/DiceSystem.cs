using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceSystem : MonoBehaviour
{
    // Declaring variables and components
    public Color diceColor;
    public int diceValue;
    public bool currentDice;
    public bool isDiceDots = true;
    public int row;
    public int column;

    public Vector3 currentPosition;
    public Vector3 DesiredPosition;

    public Color[] palatte = new Color[5];
    public Sprite[] diceDots = new Sprite[6];
    public Sprite[] diceNumber = new Sprite[6];

    private SpriteRenderer sr;
    private GameManager gm;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();

        randomDice();
    }//end of start
    
    //randomizes dices
    void randomDice() 
    {
        diceColor = palatte[Random.Range(0, 5)];
        diceValue = Random.Range(1, 7);
        sr.color = diceColor;
        if (isDiceDots)
            sr.sprite = diceDots[diceValue - 1];
        else
            sr.sprite = diceNumber[diceValue - 1];
    }//end of randomDice

    //when dice is clicked on
    void OnMouseDown() 
    {
        //moves dice to the desired place or destroy it
        gm.moveDice(row, column, currentDice);
        //StartCoroutine(diceMovement());
    }//end of onMouseDown

    IEnumerator diceMovement() 
    {
        transform.position = gm.currentDiceLocation;
        yield return null;
    
    }

}//end of DiceSystem
