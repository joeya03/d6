using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class Stats : MonoBehaviour
{
    //declaring components
    [SerializeField] private Text pointsGain;
    [SerializeField] private Text highScore;
    [SerializeField] private Text highestCombo;


    // Start is called before the first frame update
    void Start()
    {
        pointsGain = GameObject.Find("Score").GetComponent<Text>();
        highScore = GameObject.Find("HighScore").GetComponent<Text>();
        highestCombo = GameObject.Find("HighestCombo").GetComponent<Text>();

        pointsGain.text = "Score: " + PlayerPrefs.GetInt("Points");
        highScore.text = "HighScore: " + PlayerPrefs.GetInt("highScore");
        highestCombo.text = "Highest Combo: " + PlayerPrefs.GetInt("highCombo");
    }//end of Start

}//end of Stats
