using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //delcairng variables and components
    public Vector3[,] diceLocations = new Vector3[3, 5];
    public GameObject[,] dices = new GameObject[3, 5];

    public int currentColumn;

    public Vector3 currentDiceLocation;

    public GameObject diceFab;
    public GameObject currentDice;
    public PointSystem ps;
    public LiveSystem live;

    private Vector3 velocity = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponent<PointSystem>();
        live = GetComponent<LiveSystem>();

        float xValue = -7f;
        float yValue = 6.25f;
        //sets the position of the dices and instatiates them
        for (int j = 0; j < 3; j++)
        {
            xValue = -7f;
            for (int i = 0; i < 5; i++)
            {
                //create each dices
                diceLocations[j, i] = new Vector3(xValue, yValue, 0);
                dices[j, i] = Instantiate(diceFab, diceLocations[j, i], Quaternion.identity);
                dices[j, i].GetComponent<DiceSystem>().row = j;
                dices[j, i].GetComponent<DiceSystem>().column = i;
                xValue += 3.5f;
            }
            yValue -= 2.75f;
        }
    }//end of start

    // Update is called once per frame
    void Update()
    {
    }//end of update


    //checks if dices are compatable or not
    public bool checkDice(int column) 
    {
        //if there is a current dice active
        if (currentDice != null) 
        {
            if (dices[2, column].GetComponent<DiceSystem>().diceValue == currentDice.GetComponent<DiceSystem>().diceValue + 1
            || dices[2, column].GetComponent<DiceSystem>().diceColor == currentDice.GetComponent<DiceSystem>().diceColor
            || (dices[2, column].GetComponent<DiceSystem>().diceValue == 1 && currentDice.GetComponent<DiceSystem>().diceValue == 6))
            {
                Debug.Log("right");
                return true;
            }
            else
            {

                Debug.Log("left");
                return false;
            }
        }
        else
            return true;
    }//end of checkDice

    public void moveDice(int row, int column, bool current) 
    {
        currentColumn = column;
        //if row is clicked on
        if (!current)
        {
            if (checkDice(column)) 
            {
                ps.addPoint();
                //if there is a current dice active
                if (currentDice != null)
                    checkDice(column);
                //moves the dices down

                dices[0, column].transform.position = diceLocations[1, column];
                dices[1, column].transform.position = diceLocations[2, column];
                dices[2, column].transform.position = currentDiceLocation;

                //destroy dice if one is already in the middle.
                if (currentDice != null)
                    Destroy(currentDice);

                //moves the dices down the array.
                currentDice = dices[2, column];
                dices[2, column] = dices[1, column];
                dices[1, column] = dices[0, column];

                //StartCoroutine(diceMovement(column));

                //currentDice.GetComponent<DiceSystem>().row++;
                dices[0, column].GetComponent<DiceSystem>().row++;
                dices[1, column].GetComponent<DiceSystem>().row++;
                dices[2, column].GetComponent<DiceSystem>().row++;

                //sets dice from row to current dice
                currentDice.GetComponent<DiceSystem>().currentDice = true;

                //makes a new dice in that row
                dices[0, column] = Instantiate(diceFab, diceLocations[0, column], Quaternion.identity);
                dices[0, column].GetComponent<DiceSystem>().row = 0;
                dices[0, column].GetComponent<DiceSystem>().column = column;
            }
        }
        else 
        {
            //when current dice is clicked
            Destroy(currentDice);
            ps.breakCombo();
            live.loseLife();
        }
        
    }//end of moveDice

    //public void diceMovement(int column) 
    //{
    //dices[0, currentColumn].transform.position = Vector3.SmoothDamp(diceLocations[0, currentColumn], diceLocations[1, currentColumn], ref velocity, smoothSpeed);
    //dices[1, currentColumn].transform.position = Vector3.SmoothDamp(diceLocations[1, currentColumn], diceLocations[2, currentColumn], ref velocity, smoothSpeed);
    //dices[2, currentColumn].transform.position = Vector3.SmoothDamp(diceLocations[2, currentColumn], currentDiceLocation, ref velocity, smoothSpeed);

    //}

    //IEnumerator diceMovement(int column)
    //{
    //    float timeElapsed = 0;
    //    float Duration = 5;
    //    while (timeElapsed < Duration)
    //    {
    //        dices[0, column].transform.position = Vector3.Lerp(dices[0, column].transform.position, diceLocations[1, column], timeElapsed / Duration);
    //        dices[1, column].transform.position = Vector3.Lerp(dices[1, column].transform.position, diceLocations[2, column], timeElapsed / Duration);
    //        dices[2, column].transform.position = Vector3.Lerp(dices[2, column].transform.position, currentDiceLocation, timeElapsed / Duration);
    //        currentDice.transform.position = Vector3.Lerp(dices[2, column].transform.position, currentDiceLocation, timeElapsed / Duration);

    //        timeElapsed += Time.deltaTime;
    //        yield return null;
    //    }

    //}

    //checks if the player has won or lost
    public void loseGame() 
    {
        ps.highScoreCheck();
        PlayerPrefs.SetInt("Points", ps.points);
        if (ps.points >= 50)
        {
            GetComponent<SceneSystem>().LoadScene();
            Debug.Log("win");
        }
        else 
        {
            Debug.Log("lose GM");
            GetComponent<SceneSystem>().LoadScene();
        }
    
    
    }//end of loseGame
}//end of GameManager
