using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointSystem : MonoBehaviour
{
    //declaring variables and game components
    public int points;
    public int comboMeter;
    public int highestCombo;

    public int highScore;

    [SerializeField] private Slider pointSlider;
    [SerializeField] private Text pointCounter;
    [SerializeField] private Text chainCounter;

    // Start is called before the first frame update
    void Start()
    {
        points = 0;
        comboMeter = 0;
        highestCombo = PlayerPrefs.GetInt("highCombo");
        highScore = PlayerPrefs.GetInt("highScore");
        PlayerPrefs.SetInt("Points", points);


    }//end of start

    void Awake()
    {
        //grab components once present
        if (GameObject.Find("UIText"))
        {
            pointSlider = GameObject.Find("Progress").GetComponent<Slider>();
            pointCounter = GameObject.Find("Points").GetComponent<Text>();
            chainCounter = GameObject.Find("Chain").GetComponent<Text>();
        }
        else 
        {
            pointSlider = null;
            pointCounter = null;
            chainCounter = null;
        }
            
    }

    void Update()
    {
        if (GameObject.Find("UIText"))
        {
            //displays current points and combo
            pointSlider.value = points;
            pointCounter.text = points + " / 50";
            chainCounter.text = comboMeter + " CHAIN";
        }
    }//end of Update

    //adds points
    public void addPoint() 
    {
        points++;
        comboMeter++;
    }//end of addPoint

    //breaks combo and checks for a new highest combo
    public void breakCombo() 
    {
        if (comboMeter > highestCombo)
            highestCombo = comboMeter;
        comboMeter = 0;
    }//end of breakCombo

    public void highScoreCheck() 
    {
        if (highScore < points) 
        {
            PlayerPrefs.SetInt("highScore", points);
        }
        PlayerPrefs.SetInt("highCombo", highestCombo);
    
    }//end of highScore

}//end of PointSystem
