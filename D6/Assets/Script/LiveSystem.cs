using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LiveSystem : MonoBehaviour
{
    //declaring variables and components
    public int lives;

    public Image[] hearts = new Image[6];

    // Start is called before the first frame update
    void Start()
    {
        lives = 7;
    }//end of start

    //whenever current dice is clicked, lose a life
    public void loseLife() 
    {
        lives--;
        if (lives <= 0)
            gameObject.SendMessage("loseGame");
        else
            hearts[lives - 1].color = new Color(1f, 1f, 1f, 1f);
    }//end of loseLife
}
